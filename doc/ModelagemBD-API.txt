tabelas

modelagem_casalegislativa
  id*
  nome
  nome_curto (unique)
  esfera
  local

modelagem_chefeexecutivo
  id*
  nome
  genero
  mandato_ano_inicio
  mandato_ano_fim
  partido_id** (modelagem_partido)

modelagem_chefeexecutivo_casas_legislativas
  id*
  chefeexecutivo_id** (modelagem_chefe_executivo)
  casalegislativa_id** (modelagem_casalegislativa)
  ??? mandato_ano_inicio/mandato_ano_fim aqui ????

modelagem_indexadores
  id*
  termo
  principal (boolean)

modelagem_parlamentar
  id*
  parlamentar_id
  nome
  genero
  localidade
  casa_legislativa_id** (modelagem_casa_legislativa)
  partido_id** (modelagem_partido)

modelagem_partido
  id*
  nome
  numero
  cor

modelagem_proposicao
  id*
  id_prop
  sigla
  numero
  ano
  ementa
  descricao
  indexacao ??? TEXT ???
  data_apresentacao
  situacao
  autor_principal ??? NÃO DEVERIA VIR DE MODELAGEM_PARLAMENTAR ???
  casa_legislativa_id** (modelagem_casa_legislativa)

modelagem_proposicao_autores
  id*
  proposicao_id** (modelagem_proposicao)
  parlamentar_id** (modelagem_parlamentar)

modelagem_votacao
  id*
  id_vot
  descricao
  data
  resultado *NÃO ESTÁ NORMALIZADO
  proposicao_id** (modelagem_proposicao)

modelagem_voto
  id*
  opcao
  parlamentar_id** (modelagem_parlamentar - parlamentar_id)
  votacao_id** (modelagem_votacao)
